from enum import IntEnum


class EASARInstallStatus(IntEnum):
    NOT_INSTALLED=0
    INSTALLED=1
    BROKEN_LINK=2
    MISSING=3
class EIndexModStatus(IntEnum):
    NOT_INSTALLED=0
    INSTALLED=1
    NONCOMPLIANT=2

class BetterDiscordStatus:
    def __init__(self) -> None:
        self.asar_install: EASARInstallStatus = EASARInstallStatus.NOT_INSTALLED
        self.index_mod: EIndexModStatus = EIndexModStatus.NOT_INSTALLED
        self.remote_status: str = ''

    def get_asar_install_str(self) -> str:
        match self.asar_install:
            case EASARInstallStatus.NOT_INSTALLED:
                return '[red]Not installed[/red]'
            case EASARInstallStatus.INSTALLED:
                return '[green]Installed[/green]'
            case EASARInstallStatus.BROKEN_LINK:
                return '[bold red]Broken Link![/bold red]'
            case EASARInstallStatus.MISSING:
                return '[bold red]Missing![/bold red]'
        return '???'
    
    def get_index_mod_str(self) -> str:
        match self.index_mod:
            case EIndexModStatus.NOT_INSTALLED:
                return '[red]Not patched[/red]'
            case EIndexModStatus.INSTALLED:
                return '[green]Patched[/green]'
            case EIndexModStatus.NONCOMPLIANT:
                return '[bold red]Noncompliant![/bold red]'
        return '???'