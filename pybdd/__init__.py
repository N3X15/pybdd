# Excerpts from bbdctl, a bash script (https://github.com/bb010g/betterdiscordctl/blob/master/betterdiscordctl)
import json
import os
import re
import shutil
import sys
from enum import Enum, IntEnum, auto
from pathlib import Path
from typing import Any, FrozenSet, NewType, Optional, Pattern, Set, Tuple, Type, Union

import psutil
import requests

CLI_MODE: bool
Progress=Any
TypeID=int
try:
    from rich import print as rprint
    from rich.progress import Progress, TaskID

    CLI_MODE = True
except:

    def rprint(*args, style: Optional[str] = None) -> None:
        print(*args) if DEBUG_LEVEL > 0 else ""

    CLI_MODE = False

from pybdd.bddstatus import BetterDiscordStatus, EASARInstallStatus, EIndexModStatus


def get_file(p: Optional[Progress], uri: str, to_path: Path) -> None:
    fn = uri.split("/")[-1].split("?")[0]
    t: Optional[TaskID] = None
    if CLI_MODE:
        rprint(f"[cyan]Downloading[/cyan] {uri} [cyan]->[/cyan] {to_path}")
        t = p.add_task(f"[cyan]Downloading {fn}...", total=None)
    req = requests.get(uri, stream=True)
    req.raise_for_status()
    try:
        sz = int(req.headers.get("Content-Length", -1))
        if t is not None:
            if sz > 0:
                p.update(task_id=t, total=sz)
        tmpfile = to_path.with_suffix(to_path.suffix + "~")
        with tmpfile.open("wb") as f:
            for chunk in req.iter_content(chunk_size=4096):
                if t is not None:
                    p.update(t, advance=len(chunk))
                f.write(chunk)
        os.replace(tmpfile, to_path)
    finally:
        if tmpfile.is_file():
            tmpfile.unlink()
        if t is not None:
            p.remove_task(t)


def install_file(
    source_file: Path,
    dest_file: Path,
    create_parents: bool = False,
    user_name: Optional[str] = None,
    group_name: Optional[str] = None,
    file_mode: Optional[int] = None,
    keep_timestamps: bool = False,
) -> None:
    if create_parents:
        dest_file.parent.mkdir(parents=True, exist_ok=True)
    os.replace(source_file, dest_file)
    if file_mode is not None:
        dest_file.chmod(file_mode)
    if user_name is not None:
        shutil.chown(dest_file, user=user_name, group=group_name)


DEBUG_LEVEL: int = 0


class EDiscordFlavor(Enum):
    VANILLA = ""
    CANARY = "canary"
    PTB = "ptb"
    DEVELOPMENT = "development"


class EInstallType(IntEnum):
    TRADITIONAL = auto()
    SNAP = auto()
    FLATPAK = auto()


class ERemoteType(IntEnum):
    GITHUB = auto()
    URL = auto()
    DIR = auto()


REG_GET_FLAVOR_FROM_DIR: Pattern = re.compile(r"^.+/discord(.*)/\d+\.\d+\.\d+/")
REG_VERSION_DIR: Pattern = re.compile(r"(\d+)\.(\d+)\.(\d+)")


class BetterDiscordCtl:
    VERSION: Tuple[int, int, int] = (2, 0, 1)
    D_FLAVORS: FrozenSet[EDiscordFlavor] = frozenset(
        [
            EDiscordFlavor.VANILLA,
            EDiscordFlavor.CANARY,
            EDiscordFlavor.PTB,
            EDiscordFlavor.DEVELOPMENT,
        ]
    )
    BD_REMOTE: ERemoteType = ERemoteType.GITHUB
    BD_REMOTE_GITHUB_OWNER: str = "BetterDiscord"
    BD_REMOTE_GITHUB_REPO: str = "BetterDiscord"
    BD_REMOTE_GITHUB_RELEASE: str = "latest"
    BD_REMOTE_ASAR: str = "betterdiscord.asar"
    FLATPAK_BIN = "flatpak"
    SNAP_BIN = "snap"

    def __init__(self, home: Path) -> None:
        self.home: Path = home
        self.xdg_data_home = self.home / ".local" / "share"
        self.xdg_config = self.home / ".config"

        self.bd_remote_dir: Optional[Path] = None
        self.bd_remote_url: str = ""
        self.d_install: EInstallType = EInstallType.TRADITIONAL

        # Variables
        self.d_flavor: EDiscordFlavor = EDiscordFlavor.VANILLA
        self.d_core: Optional[Path] = None
        # self.xdg_config: Optional[Path] = None
        self.bdc_data: Path = self.xdg_data_home / "betterdiscordctl"
        self.d_config: Optional[Path] = None
        self.bd_config: Optional[Path] = None
        self.bd_asar: Optional[Path] = None
        self.bd_asar_escaped: str = ""
        self.bd_asar_name: str = ""

        self.d_modules: Optional[Path] = None

        self.index_js: Optional[Path] = None

        self.verbosity: int = 0

    def _die_msg(self, msg: str) -> None:
        if CLI_MODE:
            rprint(msg, style="critical")
        else:
            print(f"CRITICAL: {msg}")

    def _warn_msg(self, msg: str) -> None:
        if CLI_MODE:
            rprint(msg, style="warning")
        else:
            print(f"W: {msg}")

    def _success_msg(self, msg: str) -> None:
        if CLI_MODE:
            rprint(msg, style="success")
        else:
            print(f"SUCCESS: {msg}")

    def _verbose_msg(self, level: int, msg: str) -> None:
        if CLI_MODE:
            rprint(msg, style="verbose")
        else:
            v = "V" * level
            print(f"{v}: {msg}")

    def die(self, *msgs) -> None:
        (self._die_msg(msg) for msg in msgs)
        sys.exit(1)

    def warn(self, *msgs) -> None:
        (self._warn_msg(msg) for msg in msgs)

    def verbose(self, level: int, *msgs) -> None:
        if level > self.verbosity:
            return
        (self._verbose_msg(level, msg) for msg in msgs)

    def success(self, *msgs) -> None:
        (self._success_msg(msg) for msg in msgs)

    # https://github.com/bb010g/betterdiscordctl/blob/49a26fe82d441fbb8e51b0511969929f05cb2057/betterdiscordctl#L399
    def bdc_main(self) -> None:
        # xdg_discover_config
        self.xdg_discover_config()
        # bdc_discover
        self.bdc_discover()

        # d_core =$d_modules/discord_desktop_core
        self.d_core = self.d_modules / "discord_desktop_core"
        # [[-d $d_core]] | | die "ERROR: Directory 'discord_desktop_core' not found in: $d_modules"
        if not self.d_core.is_dir():
            self.die(
                f"ERROR: Directory 'discord_desktop_core' not found in: {self.d_modules}"
            )
        # bd_remote_init
        self.bd_remote_init()
        # bd_asar =$bd_config/data /$bd_asar_name
        self.bd_asar = self.bd_config / "data" / self.bd_asar_name
        # bd_asar_escaped =${bd_asar /\\/\\\\}
        self.bd_asar_escaped = json.dumps(str(self.bd_asar)).strip('"')

        # ADDED
        self.index_js = self.d_core / "index.js"

    def xdg_discover_config(self) -> None:
        # https://github.com/bb010g/betterdiscordctl/blob/49a26fe82d441fbb8e51b0511969929f05cb2057/betterdiscordctl#L409
        # case "$d_install" in
        #  traditional)
        if self.d_install == EInstallType.TRADITIONAL:
            # xdg_config=${XDG_CONFIG_HOME:-$HOME/.config}
            # self.xdg_config = self.xdg_config
            pass
        #  snap)
        elif self.d_install == EInstallType.SNAP:
            ## shellcheck disable=SC2016
            ## Expansion should happen inside snap's shell.
            # xdg_config=$("$snap_bin" run --shell discord \
            #    <<< $'printf -- \'%s/.config\n\' "$SNAP_USER_DATA" 1>&3' 3>&1)
            # ;;

            # > Not used.
            self.xdg_config = None
        #  flatpak)
        elif self.d_install == EInstallType.FLATPAK:
            # # shellcheck disable=SC2016
            # # Expansion should happen inside flatpak's shell.
            # xdg_config=$("$flatpak_bin" run --command=sh com.discordapp.Discord \
            #     -c $'printf -- \'%s\n\' "$XDG_CONFIG_HOME"')
            # xdg_config=${xdg_config:-$HOME/.var/app/com.discordapp.Discord/config}
            # ;;

            # > Not used.
            self.xdg_config = None

        #  *) die "ERROR: [xdg_discover_config] Unknown Discord install variant: $d_install" ;;
        else:
            self.die(
                f"ERROR: [xdg_discover_config] Unknown Discord install variant: {self.d_install!r}"
            )
        # esac
        # [[ $xdg_config ]] || >&2 printf "WARN: XDG user config directory (\$XDG_CONFIG_HOME) not found.\n"
        if self.xdg_config is None:
            self.warn(f"WARN: XDG user config directory ($XDG_CONFIG_HOME) not found.")

    def bdc_discover(self) -> None:
        # https://github.com/bb010g/betterdiscordctl/blob/49a26fe82d441fbb8e51b0511969929f05cb2057/betterdiscordctl#L432
        # d_discover_config
        self.d_discover_config()
        # bd_discover_config
        self.bd_discover_config()
        # bdc_find_modules
        self.bdc_find_modules()

    # https://github.com/bb010g/betterdiscordctl/blob/49a26fe82d441fbb8e51b0511969929f05cb2057/betterdiscordctl#L438
    def bdc_find_modules(self) -> None:
        # if [[ $d_modules ]]; then
        if self.d_modules is not None:
            # [[ -d $d_modules ]] || die "ERROR: Discord modules directory not found: $d_modules"
            if not self.d_modules.is_dir():
                self.die(
                    f"ERROR: Discord modules directory not found: {self.d_modules}"
                )

            # d_flavor=${d_modules%/*/modules}
            # d_flavor=${d_flavor##*/discord}
            m = REG_GET_FLAVOR_FROM_DIR.match(str(self.d_modules))
            self.d_flavor = EDiscordFlavor(m[1])
        # else
        else:
            # [[ -d $d_config ]] || die "ERROR: Discord $d_flavor config directory not found: $d_config"
            if not self.d_config.is_dir():
                self.die(
                    f"ERROR: Discord {self.d_flavor} config directory not found: {self.d_config}"
                )
            # declare -a all_d_modules
            # all_d_modules=("$d_config/"+([0-9]).+([0-9]).+([0-9])/modules)
            all_d_modules: Set[Path] = set()
            for entry in self.d_config.iterdir():
                if REG_VERSION_DIR.match(entry.name) is not None:
                    all_d_modules.add(entry / "modules")
            # ((${#all_d_modules[@]})) || die 'ERROR: Discord modules directory not found.' \
            # 'Try specifying it with --d-modules.'
            if len(all_d_modules) == 0:
                self.die("ERROR: Discord modules directory not found.")
            # d_modules=${all_d_modules[-1]}
            self.d_modules = all_d_modules.pop()
            self.verbose(1, f"V: Found modules in {self.d_modules}")

    def d_discover_config(self) -> None:
        # https://github.com/bb010g/betterdiscordctl/blob/49a26fe82d441fbb8e51b0511969929f05cb2057/betterdiscordctl#L459
        # [[ $xdg_config ]] || die "ERROR: XDG user config directory (\$XDG_CONFIG_HOME) not found."
        if not self.xdg_config:
            self.die("ERROR: XDG user config directory (\$XDG_CONFIG_HOME) not found.")
        # case "$d_install" in
        # traditional)
        if self.d_install == EInstallType.TRADITIONAL:
            # for d_flavor in "${d_flavors[@]}"; do
            for flavor in EDiscordFlavor:
                # verbose 2 "VV: Trying flavor '$d_flavor'"
                self.verbose(2, f"VV: Trying flavor {flavor!r}")

                # d_config=$xdg_config/discord${d_flavor,,}
                self.d_config = self.xdg_config / f"discord{flavor.value}"

                # if [[ -d $d_config ]]; then
                #   break
                if self.d_config.is_dir():
                    break

                # >&2 printf 'WARN: Discord %s config directory not found (%s).\n' \
                # "$d_flavor" "$d_config"
                self.warn(
                    f"WARN: Discord {flavor.name} config directory not found ({self.d_config})"
                )
        # snap|flatpak)
        elif self.d_install in (EInstallType.SNAP, EInstallType.FLATPAK):
            # d_config=$xdg_config/discord
            self.d_config = self.xdg_config / "discord"
            # if [[ ! -d $d_config ]]; then
            if not self.d_config.is_dir():
                # >&2 printf 'WARN: Discord %s config directory not found (%s).\n' \
                # "$d_install" "$d_config"
                self.warn(
                    f"WARN: Discord {flavor.name} config directory not found ({self.d_config})"
                )

        # *) die "ERROR: [d_discover_config] Unknown Discord install variant: $d_install" ;;
        else:
            self.die(
                f"ERROR: [d_discover_config] Unknown Discord install variant: {self.d_install!r}"
            )

    # https://github.com/bb010g/betterdiscordctl/blob/49a26fe82d441fbb8e51b0511969929f05cb2057/betterdiscordctl#L484
    def bd_discover_config(self) -> None:
        # [[$xdg_config]] || die "ERROR: XDG user config directory (\$XDG_CONFIG_HOME) not found."
        if not self.xdg_config:
            self.die("ERROR: XDG user config directory (\$XDG_CONFIG_HOME) not found.")
        # case "$d_install" in
        # traditional | snap | flatpak)
        if self.d_install in (
            EInstallType.FLATPAK,
            EInstallType.SNAP,
            EInstallType.TRADITIONAL,
        ):
            # bd_config =$xdg_config/BetterDiscord
            self.bd_config = self.xdg_config / "BetterDiscord"
        #     ;;
        # *) die "ERROR: [bd_discover_config] Unknown Discord install variant: $d_install" ;;
        # esac
        else:
            self.die(
                f"ERROR: [bd_discover_config] Unknown Discord install variant: {self.d_install!r}"
            )

    # https://github.com/bb010g/betterdiscordctl/blob/49a26fe82d441fbb8e51b0511969929f05cb2057/betterdiscordctl#L496
    def bd_remote_init(self) -> None:
        # case "$bd_remote" in
        #   github) bd_remote_init_github ;;
        if self.BD_REMOTE == ERemoteType.GITHUB:
            self.bd_remote_init_github()
        #   url) bd_remote_init_url ;;
        elif self.BD_REMOTE == ERemoteType.URL:
            self.bd_remote_init_url()
        #   dir) bd_remote_init_dir ;;
        elif self.BD_REMOTE == ERemoteType.DIR:
            self.bd_remote_init_dir()
        #   *) die "ERROR: [bd remote init] Unknown remote type: $bd_remote" ;;
        # esac
        else:
            self.die(f"ERROR: [bd remote init] Unknown remote type: {self.BD_REMOTE!r}")
        # verbose 2 "VV: BetterDiscord remote asar path: $bd_remote_asar"
        self.verbose(2, f"VV: BetterDiscord remote asar path: {self.BD_REMOTE_ASAR}")
        # bd_asar_name=${bd_remote_asar##*/}
        self.bd_asar_name = Path(self.BD_REMOTE_ASAR).name

    def bd_remote_init_github(self) -> None:
        # bd_remote_init_github() {
        #   bd_remote_url=https://github.com/$bd_remote_github_owner/$bd_remote_github_repo/releases/$bd_remote_github_release/download
        #   verbose 2 "VV: BetterDiscord remote GitHub repository owner: $bd_remote_github_owner"
        #   verbose 2 "VV: BetterDiscord remote GitHub repository name: $bd_remote_github_repo"
        #   verbose 2 "VV: BetterDiscord remote GitHub repository release: $bd_remote_github_release"
        #   bd_remote_init_url
        # }
        self.bd_remote_url = f"https://github.com/{self.BD_REMOTE_GITHUB_OWNER}/{self.BD_REMOTE_GITHUB_REPO}/releases/{self.BD_REMOTE_GITHUB_RELEASE}/download"
        self.verbose(
            2,
            f"VV: BetterDiscord remote GitHub repository owner: {self.BD_REMOTE_GITHUB_OWNER}",
        )
        self.verbose(
            2,
            f"VV: BetterDiscord remote GitHub repository name: {self.BD_REMOTE_GITHUB_REPO}",
        )
        self.verbose(
            2,
            f"VV: BetterDiscord remote GitHub repository release: {self.BD_REMOTE_GITHUB_RELEASE}",
        )
        self.bd_remote_init_url()

    def bd_remote_init_url(self) -> None:
        # bd_remote_dir =$bd_config/data
        self.bd_remote_dir = self.bd_config / "data"
        # verbose 2 "VV: BetterDiscord remote URL: $bd_remote_url"
        self.verbose(2, f"VV: BetterDiscord remote URL: {self.bd_remote_url}")
        # bd_remote_init_dir
        self.bd_remote_init_dir()

    def bd_remote_init_dir(self) -> None:
        self.verbose(2, f"VV: BetterDiscord remote directory: {self.bd_remote_dir}")

    # https://github.com/bb010g/betterdiscordctl/blob/49a26fe82d441fbb8e51b0511969929f05cb2057/betterdiscordctl#L283
    def bdc_status(self) -> BetterDiscordStatus:
        # declare asar_install bd_remote_status index_mod
        status = BetterDiscordStatus()
        # asar_install = no
        # index_mod = no
        # verbose 2 "VV: BetterDiscord asar installation: $bd_asar"
        self.verbose(2, f"VV: BetterDiscord asar installation: {self.bd_asar}")
        # if [[-h $bd_asar & & ! -f $bd_asar]]; then
        if self.bd_asar.is_symlink() and not self.bd_asar.is_file():
            # asar_install = '(broken link) no'
            status.asar_install = EASARInstallStatus.BROKEN_LINK
        # elif [[-f $bd_asar]]; then
        elif self.bd_asar.is_file():
            # asar_install = '(symbolic link) yes'
            status.asar_install = EASARInstallStatus.INSTALLED
        # elif [[-d $bd_config]]; then
        elif self.bd_config.is_dir():
            # asar_install = '(missing) no'
            status.asar_install = EASARInstallStatus.MISSING
        # if grep -Fq "$bd_asar_escaped" "$d_core/index.js"; then
        #     index_mod = yes
        #   elif grep -Fq "$bd_asar_name" "$d_core/index.js"; then
        #     index_mod = noncompliant
        #   elif grep -Fq 'betterdiscord.asar' "$d_core/index.js"; then
        #     index_mod = noncompliant
        #   fi

        # This whole thing was a mess, so I redid it.
        index_js_content: str = self.index_js.read_text()
        if self.bd_asar_escaped in index_js_content:
            # index_mod = 'yes'
            status.index_mod = EIndexModStatus.INSTALLED
        elif self.bd_asar_name in index_js_content:
            # index_mod = 'noncompliant'
            status.index_mod = EIndexModStatus.NONCOMPLIANT
        elif "betterdiscord.asar" in index_js_content:
            # index_mod = 'noncompliant'
            status.index_mod = EIndexModStatus.NONCOMPLIANT

        # This was also a mess.
        match self.BD_REMOTE:
            case ERemoteType.GITHUB:
                status.remote_status = f"GitHub: ~{self.BD_REMOTE_GITHUB_OWNER}/{self.BD_REMOTE_GITHUB_REPO}#{self.BD_REMOTE_GITHUB_RELEASE}"
            case ERemoteType.URL:
                status.remote_status = f"URL: ${self.bd_remote_url}"
            case ERemoteType.DIR:
                status.remote_status = f"Directory: ${self.bd_remote_dir}"

        return status

    def bdc_install(self) -> None:
        # grep -Fq "$bd_asar_escaped" "$d_core/index.js" && die 'ERROR: Already installed.'
        if self.bd_asar_escaped in self.index_js.read_text():
            self.die("ERROR: Already installed.")
        # bdc_clean_legacy
        self.bdc_clean_legacy()

        # bd_remote_install
        self.bd_remote_install()

        # bd_install
        self.bd_install()

        # >&2 printf 'Installed. (Restart Discord if necessary.)\n'
        self.success("Installed. (Restart Discord if necessary.)")

    def bdc_reinstall(self) -> None:
        # grep -Fq "$bd_asar_name" "$d_core/index.js" || die 'ERROR: Not installed.'
        if self.bd_asar_name in self.index_js.read_text():
            self.die("ERROR: Not installed.")

        # bdc_clean_legacy
        self.bdc_clean_legacy()

        # bdc_kill
        self.bdc_kill()

        # bd_remote_install
        self.bd_remote_install()
        # bd_install
        self.bd_install()

        # >&2 printf 'Reinstalled.\n'
        self.success("Reinstalled.")

    def bdc_kill(self) -> None:
        # >&2 printf 'Killing Discord %s processes...\n' "$d_flavor"
        self.verbose(1, f"Killing Discord {self.d_flavor.value} processes...")

        # pkill -exi -KILL "discord${d_flavor:0:8}" || >&2 printf 'No active processes found.\n'
        procNameNeedle: str = f"discord{self.d_flavor.value[:8]}"
        proc: psutil.Process
        for proc in psutil.process_iter():
            try:
                if proc.name() == procNameNeedle:
                    # Added
                    self.verbose(1, f" -> Killing process #{proc.pid}")
                    proc.kill()
            except (psutil.NoSuchProcess, psutil.AccessDenied, psutil.ZombieProcess):
                pass

    def bdc_clean_legacy(self) -> None:
        """
        bdc_clean_legacy() {
        if [[ -d $d_core/core ]]; then
            >&2 printf 'Removing legacy core directory...\n'
            rm -rf "$d_core/core"
        fi
        if [[ -d $d_core/injector ]]; then
            >&2 printf 'Removing legacy injector directory...\n'
            rm -rf "$d_core/injector"
        fi
        if [[ -d $bdc_data ]]; then
            if [[ -f "$bdc_data/bd_map" || -d "$bdc_data/bd" ]]; then
            >&2 printf 'Removing legacy machine-specific data...\n'
            rm -rf "$bdc_data/bd_map" "$bdc_data/bd"
            fi
        fi
        }
        """
        core_core_dir: Path = self.d_core / "core"
        core_injector_dir: Path = self.d_core / "injector"
        if core_core_dir.is_dir():
            self.verbose(1, "Removing legacy core directory...")
            shutil.rmtree(core_core_dir)
        if core_injector_dir.is_dir():
            self.verbose(1, "Removing legacy injector directory...")
            shutil.rmtree(core_injector_dir)
        if self.bdc_data.is_dir():
            bd_map_file: Path = self.bdc_data / "bd_map"
            bd_dir: Path = self.bdc_data / "bd"
            if bd_map_file.is_file() or bd_dir.is_dir():
                self.verbose(1, "Removing legacy machine-specific data...")
                bd_map_file.unlink(missing_ok=True)
                if bd_dir.is_dir():
                    shutil.rmtree(core_injector_dir)

    def bd_install(self) -> None:
        """
        bd_install() {
            verbose 1 'V: Injecting into index.js...'
            printf $'require("%s");
            module.exports = require(\'./core.asar\');
            ' "$bd_asar_escaped" > "$d_core/index.js"
        }
        """
        self.verbose(1, "Injecting into index.js...")
        self.index_js.write_text(
            f"""require({json.dumps(str(self.bd_asar))});
module.exports = require('./core.asar');
"""
        )

    def bd_uninstall(self) -> None:
        """
                bd_uninstall() {
                    verbose 1 'V: Removing BetterDiscord injection...'
                    printf $'module.exports = require(\'./core.asar\');
        ' > "$d_core/index.js"
                }
        """
        self.verbose(1, "Removing BetterDiscord injection...")
        self.index_js.write_text("module.exports = require('./core.asar')\n")

    def bd_remote_install(self) -> None:
        """
        bd_remote_install() {
            case "$bd_remote" in
                github) bd_remote_install_github ;;
                url) bd_remote_install_url ;;
                dir) bd_remote_install_dir ;;
                *) die "ERROR: [bd remote install] Unknown remote type: $bd_remote" ;;
            esac
        }
        """
        if self.BD_REMOTE == ERemoteType.GITHUB:
            self.bd_remote_install_github()
        elif self.BD_REMOTE == ERemoteType.URL:
            self.bd_remote_install_url()
        elif self.BD_REMOTE == ERemoteType.DIR:
            self.bd_remote_install_dir()
        else:
            self.die(f"Unknown remote type: {self.BD_REMOTE!r}")

    def bd_remote_install_github(self) -> None:
        """
        bd_remote_install_github() {
            verbose 2 "VV: Installing remote BetterDiscord (GitHub)..."
            bd_remote_install_url
        }
        """
        self.verbose(2, "Installing remote BetterDiscord (GitHub)...")
        self.bd_remote_install_url()

    def bd_remote_install_url(self) -> None:
        """
        bd_remote_install_url() {
            verbose 2 "VV: Installing remote BetterDiscord (URL)..."
            verbose 1 "V: Downloading BetterDiscord asar..."
            curl -LSso "$bd_remote_dir/$bd_remote_asar" --create-dirs \
                "$bd_remote_url/$bd_remote_asar"
            bd_remote_install_dir
        }
        """
        self.verbose(2, "Installing remote BetterDiscord (URL)...")
        self.verbose(1, "Downloading BetterDiscord asar...")
        target_asar: Path = self.bd_remote_dir / self.BD_REMOTE_ASAR
        target_asar.parent.mkdir(parents=True, exist_ok=True)
        if CLI_MODE:
            with Progress() as p:
                get_file(p, self.bd_remote_url + "/" + self.BD_REMOTE_ASAR, target_asar)
        else:
            get_file(None, self.bd_remote_url + "/" + self.BD_REMOTE_ASAR, target_asar)
        self.bd_remote_install_dir()

    def bd_remote_install_dir(self) -> None:
        """
        bd_remote_install_dir() {
            verbose 2 "VV: Installing remote BetterDiscord (directory)..."
            if [[ "$bd_remote_dir/$bd_remote_asar" != "$bd_asar" ]]; then
                verbose 1 "V: Copying BetterDiscord asar..."
                install -Dm 644 "$bd_remote_dir/$bd_remote_asar" "$bd_asar"
            fi
        }
        """
        target_asar: Path = self.bd_remote_dir / self.BD_REMOTE_ASAR
        self.verbose(2, "Installing remote BetterDiscord (directory)...")
        if str(target_asar) != self.bd_asar:
            self.verbose(1, "Copying BetterDiscord asar...")
            # install -Dm 644 "$bd_remote_dir/$bd_remote_asar" "$bd_asar"
            # os_utils.single_copy(target_asar, self.bd_asar, as_file=True)
            install_file(
                self.bd_asar, target_asar, create_parents=True, file_mode=0o644
            )

    # ADDED
    def is_installed(self) -> bool:
        if self.index_js is None:
            self.bdc_main()
        bds = self.get_status()
        if bds.asar_install != EASARInstallStatus.INSTALLED:
            return False
        if bds.index_mod != EIndexModStatus.INSTALLED:
            return False
        # TODO: Check repo status.
        return self.bd_asar_escaped in self.index_js.read_text()

    # ADDED
    def get_status(self) -> BetterDiscordStatus:
        self.bdc_main()
        return self.bdc_status()

    # ADDED
    def install(self) -> None:
        self.bdc_main()
        self.bdc_install()

    # ADDED
    def reinstall(self) -> None:
        self.bdc_main()
        self.bdc_reinstall()
