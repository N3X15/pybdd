from typing import Any
from rich.console import Console as RichConsole
from rich.style import Style
__all__ = ['Console']

class Console(RichConsole):
    ERRSTYLE: Style = Style(color='red', bold=True)
    def error(self, *msg: Any) -> None:
        self.print(*msg, style=self.ERRSTYLE)