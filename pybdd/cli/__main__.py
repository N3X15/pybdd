import argparse
import os
from pathlib import Path

from pybdd import BetterDiscordCtl
from rich.table import Table
from pybdd.console import Console

def main():
    global DEBUG_LEVEL
    argp = argparse.ArgumentParser()
    argp.add_argument("--verbose", "-v", action="count", default=0)

    subp = argp.add_subparsers()

    _register_status(subp)

    args = argp.parse_args()
    if not hasattr(args, "cmd"):
        argp.print_usage()
        return

    DEBUG_LEVEL = max(0, args.verbose)
    # print(f'DEBUG_LEVEL={DEBUG_LEVEL}')
    args.cmd(args)


def _register_status(subp: argparse._SubParsersAction) -> None:
    p: argparse.ArgumentParser = subp.add_parser("status")
    p.add_argument("--home", type=Path, default=Path(os.environ["HOME"]))
    p.set_defaults(cmd=cmd_status)

def cmd_status(args: argparse.Namespace) -> None:
    console = Console()
    ctl = BetterDiscordCtl(args.home)
    status = ctl.get_status()
    t = Table(title='BDD Status', show_header=False)
    t.add_row('Discord Install', ctl.d_install.name)
    t.add_row('Discord Flavor', ctl.d_flavor.name)
    t.add_row('Discord Modules', str(ctl.d_modules))
    t.add_row('BetterDiscord Directory', str(ctl.bd_config))
    t.add_row('BetterDiscord Remote', status.remote_status)
    t.add_section()
    t.add_row('ASAR', status.get_asar_install_str())
    t.add_row('index.js Patch', status.get_index_mod_str())
    console.print(t)

if __name__ == "__main__":
    main()
