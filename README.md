# pybdd

A port of [betterdiscordctl](https://github.com/bb010g/betterdiscordctl/blob/master/betterdiscordctl) from bash to Python.

`pybdd` is a library to enable Python to manage BetterDiscord-enabled Discord installations programmatically. This primarily powers a private discord jailing apparatus of mine.
